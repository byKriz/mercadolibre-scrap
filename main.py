import psycopg2
from Scrap.controller import Controller

url = 'https://carros.mercadolibre.com.ve/acc-camionetas/'
conection = psycopg2.connect(user='kriz', password='admin', host='localhost', port='5432', database='test_db')

if __name__ == '__main__':
    article = Controller(url)
    values = article.run(1)

    try:
        with conection:
            with conection.cursor() as cursor:
                sentence = 'INSERT INTO ml_products(name, price, sells, seller_name, seller_rate, seller_time, total_sells, link) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)'
                cursor.executemany(sentence, values)
                registers = cursor.rowcount
                print(f'Total registers: {registers}')
    except Exception as e:
        print(f'Error Type: {e}')
    finally:
        conection.close()





