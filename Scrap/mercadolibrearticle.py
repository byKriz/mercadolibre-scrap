import requests
from bs4 import BeautifulSoup

class MercadoLibreArticle:

    def __init__(self, url):
        self.__url = url
        self.__HEADERS = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'}

    def run(self):
        r = requests.get(self.__url, headers=self.__HEADERS)
        soup = BeautifulSoup(r.text, 'html.parser')
        name = soup.find('div', {'class': 'ui-pdp-header__title-container'}).text
        price = soup.find('div', {'class': 'ui-pdp-price__second-line'}).find('span', {'class': 'price-tag-text-sr-only'}).text
        sales = soup.find('div', {'class': 'ui-pdp-header__subtitle'}).find('span', {'class': 'ui-pdp-subtitle'}).text

        # Seller Data
        def sealer_name(soup):
            try:
                name = soup.find('div', {'class': 'ui-pdp-seller__header__title'}).text
            except Exception:
                name = 'No Info'
            return name

        seller_data = soup.find('div', {'class': 'ui-pdp-seller__reputation-info'}).find_all('strong', {'class': 'ui-pdp-seller__sales-description'})
        seller_rate = seller_data[0].text
        seller_time = seller_data[1].text
        total_sells = seller_data[2].text

        # clean functions
        name_clean = lambda x: x.replace('Agregar a favoritos', '')
        price_clean = lambda x: str(float(x.replace(' dólares', '').replace(' con ', '.').replace(' centavos ', '')))
        sales_clean = lambda x: str(int(x.replace('Nuevo  |  ', '').replace(' vendidos', '').replace('Usado  |  ', '')))

        return name_clean(name), price_clean(price), sales_clean(sales), sealer_name(soup), seller_rate, seller_time, total_sells, self.__url













