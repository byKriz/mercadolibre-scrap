import requests
from bs4 import BeautifulSoup
from Scrap.mercadolibrearticle import MercadoLibreArticle
from time import sleep
from random import uniform

class Controller:

    def __init__(self, url):
        self.__url = url
        self.__HEADERS =  {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'}

    def __articles_links(self, url) -> list:
        r = requests.get(url, headers=self.__HEADERS)
        soup = BeautifulSoup(r.text, 'html.parser')
        products = soup.find('ol', {'class': 'ui-search-layout ui-search-layout--stack'}).find_all('li', {'class': 'ui-search-layout__item'})
        raw_link = lambda x: x.find('div', {'class': 'ui-search-item__group ui-search-item__group--title'}).find('a')['href']
        links = list(map(raw_link, products))
        return links

    def __paging(self, url):
        r = requests.get(url, headers=self.__HEADERS)
        soup = BeautifulSoup(r.text, 'html.parser')
        next_button = soup.find('div', {'class': 'ui-search-pagination'}).find('li', {'class': 'andes-pagination__button andes-pagination__button--next'}).find('a')['href']
        return next_button

    def run(self, pages=5):
        total_data = []
        url = self.__url
        n = 1

        try:
            for i in range(pages):
                products = self.__articles_links(url)
                for product in products:
                    article = MercadoLibreArticle(product)
                    data = article.run()
                    print(n, data)
                    total_data.append(data)
                    n += 1
                    if pages > 5:
                        sleep(uniform(2, 5))
                url = self.__paging(url)
            return tuple(total_data)
        except Exception as e:
            print(f'Ocurrio un error en el producto {n} de tipo: {e}')
            return tuple(total_data)





